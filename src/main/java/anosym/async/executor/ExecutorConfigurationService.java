package anosym.async.executor;

import javax.annotation.Nonnull;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Jul 10, 2018, 8:50:06 PM
 */
public interface ExecutorConfigurationService {

  @Nonnull
  int getPoolSize(@Nonnull final String poolName);

  @Nonnull
  int getMaxPoolSize(@Nonnull final String poolName);

  @Nonnull
  int getKeepAliveTime(@Nonnull final String poolName);

}
