package anosym.async.executor;

import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;

import anosym.async.PropagatedContext;
import anosym.async.PropagatedContextExecutor;
import anosym.async.PropagatedContextFactory;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import lombok.NonNull;

/**
 * To be used with CompletableFutures, in order to propagate correct context in application.
 *
 * @author marembo (marembo2008@gmail.com)
 * @since May 13, 2018, 10:15:16 AM
 */
@ApplicationScoped
public class ContextManagedExecutor implements Executor {

  @Inject
  private ExecutorService executorService;

  @Inject
  private PropagatedContextExecutor propagatedContextExecutor;

  @Inject
  private PropagatedContextFactory propagatedContextFactory;

  @Override
  public void execute(@NonNull final Runnable command) {
    final PropagatedContext propagatedContext = propagatedContextFactory.createPropagatedContext();
    executorService.execute(() -> propagatedContextExecutor.execute(propagatedContext, command));
  }

}
