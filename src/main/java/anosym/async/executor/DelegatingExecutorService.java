package anosym.async.executor;

import java.util.Collection;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import javax.annotation.Nonnull;

import anosym.async.PropagatedContext;
import anosym.async.PropagatedContextExecutor;
import anosym.async.PropagatedContextFactory;
import lombok.AllArgsConstructor;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;

/**
 * To be used with CompletableFutures, in order to propagate correct context in application.
 *
 * @author marembo (marembo2008@gmail.com)
 * @since May 13, 2018, 10:15:16 AM
 */
@Slf4j
@AllArgsConstructor
public class DelegatingExecutorService implements ExecutorService {

    @Nonnull
    private final ExecutorService delegate;

    @Nonnull
    private final PropagatedContextExecutor asynchronousServicePropagatedContext;

    @Nonnull
    private final PropagatedContextFactory propagatedContextFactory;

    @Override
    public void execute(@NonNull final Runnable command) {
        final PropagatedContext propagatedContext = propagatedContextFactory.createPropagatedContext();

        log.debug("Propagating context for async call: {}", propagatedContext);

        delegate.execute(() -> asynchronousServicePropagatedContext.execute(propagatedContext, command));
    }

    @Override
    public <T> Future<T> submit(Callable<T> task) {
        return delegate.submit(task);
    }

    @Override
    public <T> Future<T> submit(Runnable task, T result) {
        return delegate.submit(task, result);
    }

    @Override
    public Future<?> submit(Runnable task) {
        return delegate.submit(task);
    }

    @Override
    public <T> List<Future<T>> invokeAll(Collection<? extends Callable<T>> tasks) throws InterruptedException {
        return delegate.invokeAll(tasks);
    }

    @Override
    public <T> List<Future<T>> invokeAll(Collection<? extends Callable<T>> tasks, long timeout, TimeUnit unit) throws InterruptedException {
        return delegate.invokeAll(tasks, timeout, unit);
    }

    @Override
    public <T> T invokeAny(Collection<? extends Callable<T>> tasks) throws InterruptedException, ExecutionException {
        return delegate.invokeAny(tasks);
    }

    @Override
    public <T> T invokeAny(Collection<? extends Callable<T>> tasks, long timeout, TimeUnit unit) throws InterruptedException, ExecutionException, TimeoutException {
        return delegate.invokeAny(tasks, timeout, unit);
    }

    @Override
    public void shutdown() {
        delegate.shutdown();
    }

    @Override
    public List<Runnable> shutdownNow() {
        return delegate.shutdownNow();
    }

    @Override
    public boolean isShutdown() {
        return delegate.isShutdown();
    }

    @Override
    public boolean isTerminated() {
        return delegate.isTerminated();
    }

    @Override
    public boolean awaitTermination(long timeout, TimeUnit unit) throws InterruptedException {
        return delegate.awaitTermination(timeout, unit);
    }

}
