package anosym.async.executor;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.ThreadFactory;

import jakarta.annotation.Resource;
import jakarta.enterprise.concurrent.ManagedExecutorService;
import jakarta.enterprise.concurrent.ManagedThreadFactory;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.enterprise.inject.Default;
import jakarta.enterprise.inject.Produces;

/**
 *
 * @author marembo
 */
@ApplicationScoped
public class ManagedExecutorContextProvider {

  @Resource
  private ManagedExecutorService managedExecutorService;

  @Resource
  private ManagedThreadFactory managedThreadFactory;

  @Default
  @Produces
  @ApplicationScoped
  public ExecutorService getManagedExecutorService() {
    return managedExecutorService;
  }

  @Produces
  @ApplicationScoped
  public ThreadFactory getManagedThreadFactory() {
    return managedThreadFactory;
  }

}
