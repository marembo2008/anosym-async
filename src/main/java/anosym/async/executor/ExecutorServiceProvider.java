package anosym.async.executor;

import java.util.Map;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import javax.annotation.Nonnull;

import com.google.common.collect.ImmutableList;

import anosym.async.PropagatedContextExecutor;
import anosym.async.PropagatedContextFactory;
import jakarta.annotation.PreDestroy;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.enterprise.context.Dependent;
import jakarta.enterprise.inject.Instance;
import jakarta.enterprise.inject.Produces;
import jakarta.enterprise.inject.spi.InjectionPoint;
import jakarta.inject.Inject;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;

import static com.google.common.base.MoreObjects.firstNonNull;
import static com.google.common.base.Strings.emptyToNull;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since May 16, 2018, 10:59:54 PM
 */
@Slf4j
@ApplicationScoped
public class ExecutorServiceProvider {

  private static final ExecutorConfigurationService DEFAULT_CONFIG = new DefaultExecutorConfigurationService();

  @Inject
  private Instance<ThreadFactory> managedThreadFactory;

  @Inject
  private PropagatedContextExecutor propagatedContextExecutor;

  @Inject
  private PropagatedContextFactory propagatedContextFactory;

  @Inject
  private Instance<ExecutorConfigurationService> executorConfigurationServices;

  private final Map<String, ExecutorService> EXECUTORS = new ConcurrentHashMap<>();

  @PreDestroy
  void stopAllExecutors() {
    EXECUTORS.forEach((name, executor) -> {
      log.info("Stopping Executor: {}", name);
      executor.shutdownNow();
    });

    log.info("Completed executors shutdown: {}", EXECUTORS.keySet());
  }

  @Executor
  @Produces
  @Dependent
  public ExecutorService definedExecutorService(final InjectionPoint ip) {
    final Executor executor = ip.getAnnotated().getAnnotation(Executor.class);
    final String executorName = firstNonNull(emptyToNull(executor.value()), "default");
    if (executorConfigurationServices.isUnsatisfied()) {
      log.warn("No executor configuration defined for <{}>. using default configurations");
    }

    return EXECUTORS.computeIfAbsent(executorName, this::defineExecutorService);
  }

  @Nonnull
  private ExecutorService defineExecutorService(@NonNull final String executorName) {
    log.debug("Defining Delegating Executor Service for: {}", executorName);

    final ExecutorConfigurationService executorConfigurationService = ImmutableList
            .copyOf(executorConfigurationServices)
            .stream()
            .findFirst()
            .orElse(DEFAULT_CONFIG);
    final int corePoolSize = executorConfigurationService.getPoolSize(executorName);
    final int maxPoolsize = executorConfigurationService.getMaxPoolSize(executorName);
    final int keepAliveTime = executorConfigurationService.getKeepAliveTime(executorName);
    final ExecutorService delegate = new ThreadPoolExecutor(
            corePoolSize,
            maxPoolsize,
            keepAliveTime,
            TimeUnit.SECONDS,
            new ArrayBlockingQueue<>(maxPoolsize),
            managedThreadFactory.get());
    return new DelegatingExecutorService(delegate, propagatedContextExecutor, propagatedContextFactory);
  }

  private static final class DefaultExecutorConfigurationService implements ExecutorConfigurationService {

    @Override
    public int getKeepAliveTime(String poolName) {
      return 5;
    }

    @Override
    public int getMaxPoolSize(String poolName) {
      return 10;
    }

    @Override
    public int getPoolSize(String poolName) {
      return 5;
    }

  }

}
