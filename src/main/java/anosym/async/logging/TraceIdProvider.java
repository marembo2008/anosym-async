package anosym.async.logging;

import lombok.NonNull;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since May 16, 2018, 11:44:32 PM
 */
public interface TraceIdProvider {

    @NonNull
    TraceId getTraceId();

    void setTraceId(@NonNull final TraceId traceId);

    void removeTraceId(@NonNull final TraceId traceId);

}
