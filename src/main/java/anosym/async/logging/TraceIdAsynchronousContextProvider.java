package anosym.async.logging;

import static anosym.async.typesafe.TypeSafeKeys.key;

import java.util.Optional;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.MDC;

import anosym.async.context.AsynchronousContext;
import anosym.async.context.AsynchronousContextConsumer;
import anosym.async.context.AsynchronousContextRemover;
import anosym.async.context.AsynchronousContextSupplier;
import anosym.async.context.ForAsynchronousContext;
import anosym.async.typesafe.TypeSafeKey;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Feb 5, 2017, 11:20:36 AM
 */
@Slf4j
@ApplicationScoped
@ForAsynchronousContext(TraceId.class)
class TraceIdAsynchronousContextProvider implements AsynchronousContextSupplier<TraceId>,
                                                    AsynchronousContextConsumer<TraceId>,
                                                    AsynchronousContextRemover<TraceId> {

  private static final TypeSafeKey<TraceId> TRACE_ID_CONTEXT = key("propagated-context.traceId", TraceId.class);

  @Inject
  private TraceIdProvider traceIdProvider;

  @Override
  public Optional<AsynchronousContext<TraceId>> provide() {
    final AsynchronousContext<TraceId> asynchronousContext = AsynchronousContext.<TraceId>builder()
            .context(traceIdProvider.getTraceId())
            .contextKey(TRACE_ID_CONTEXT)
            .build();
    return Optional.of(asynchronousContext);
  }

  @Override
  public void consume(@NonNull final AsynchronousContext<TraceId> asynchronousContext) {
    final TraceId traceId = asynchronousContext.getContext();
    traceIdProvider.setTraceId(traceId);
    MDC.put("Trace-Id", traceId.getId().toString());
  }

  @Override
  public void remove(@NonNull final AsynchronousContext<TraceId> asynchronousContext) {
    final TraceId traceId = asynchronousContext.getContext();
    traceIdProvider.removeTraceId(traceId);
    MDC.remove("Trace-Id");
  }

}
