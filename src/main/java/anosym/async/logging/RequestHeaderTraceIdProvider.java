package anosym.async.logging;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.enterprise.context.ContextNotActiveException;
import jakarta.enterprise.inject.Instance;
import jakarta.inject.Inject;
import lombok.NonNull;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since May 13, 2018, 9:54:33 AM
 */
@ApplicationScoped
class RequestHeaderTraceIdProvider implements TraceIdProvider {

  private static final ThreadLocal<TraceId> TRACE_ID = new ThreadLocal<>();

  @Inject
  private Instance<RequestContextTraceIdRegistrar> requestContextTraceIdRegistrar;

  @NonNull
  @Override
  public TraceId getTraceId() {
    return getTraceIdFromRegistrarOrLocal();
  }

  @Override
  public void setTraceId(@NonNull final TraceId traceId) {
    TRACE_ID.set(traceId);
  }

  @Override
  public void removeTraceId(@NonNull final TraceId traceId) {
    TRACE_ID.remove();
  }

  @NonNull
  private TraceId getTraceIdFromRegistrarOrLocal() {
    try {
      return requestContextTraceIdRegistrar.get().getRequestTraceId();
    } catch (final ContextNotActiveException ex) {
      return setTraceIdIfNotExists(TraceId.createTraceId());
    }
  }

  @NonNull
  private TraceId setTraceIdIfNotExists(@NonNull final TraceId traceId) {
    final TraceId currentTraceId = TRACE_ID.get();
    if (currentTraceId != null) {
      return currentTraceId;
    }

    TRACE_ID.set(traceId);
    return traceId;
  }

}
