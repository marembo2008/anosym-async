package anosym.async.logging;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.servlet.ServletRequestEvent;
import jakarta.servlet.ServletRequestListener;
import jakarta.servlet.annotation.WebListener;
import jakarta.servlet.http.HttpServletRequest;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.MDC;

import static com.google.common.base.Strings.isNullOrEmpty;

/**
 * Registers trace id on request, if available.
 *
 * @author marembo (marembo2008@gmail.com)
 * @since May 13, 2018, 9:55:49 AM
 */
@Slf4j
@WebListener
@ApplicationScoped
public class RequestContextTraceIdRegistrar implements ServletRequestListener {

  private static final String TRACE_ID_HEADER = "X-Trace-Id";

  private static final ThreadLocal<TraceId> REQUEST_TRACE_ID = ThreadLocal.withInitial(TraceId::createTraceId);

  @Override
  public void requestDestroyed(@NonNull final ServletRequestEvent sre) {
    REQUEST_TRACE_ID.remove();
  }

  @Override
  public void requestInitialized(@NonNull final ServletRequestEvent sre) {
    final HttpServletRequest servletRequest = (HttpServletRequest) sre.getServletRequest();
    final TraceId traceId = getTraceIdOrDefault(servletRequest);
    MDC.put("Trace-Id", traceId.getId().toString());

    REQUEST_TRACE_ID.set(traceId);
  }

  @NonNull
  public TraceId getRequestTraceId() {
    return REQUEST_TRACE_ID.get();
  }

  @NonNull
  private TraceId getTraceIdOrDefault(@NonNull final HttpServletRequest servletRequest) {
    final String reqTraceId = servletRequest.getHeader(TRACE_ID_HEADER);
    if (isNullOrEmpty(reqTraceId)) {
      final TraceId newTraceId = REQUEST_TRACE_ID.get();

      log.debug("No Trace-Id in request, generating a new one: {}", newTraceId);

      return newTraceId;
    }

    log.debug("Setting Trace-Id from request: {}", reqTraceId);

    return TraceId.fromId(Long.parseLong(reqTraceId));
  }

}
