package anosym.async.logging;

import java.util.UUID;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NonNull;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since May 13, 2018, 9:50:13 AM
 */
@Data
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class TraceId {

    @NonNull
    private final Long id;

    @NonNull
    public static TraceId createTraceId() {
        return new TraceId(UUID.randomUUID().getMostSignificantBits());
    }

    @NonNull
    public static TraceId fromId(@NonNull final Long id) {
        return new TraceId(id);
    }

}
