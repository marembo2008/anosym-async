package anosym.async;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.annotation.Nonnull;

import com.google.common.collect.ImmutableList;

import anosym.async.context.AsynchronousContext;
import anosym.async.context.AsynchronousContextSupplier;
import anosym.profiler.Profile;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.enterprise.inject.Any;
import jakarta.enterprise.inject.Instance;
import jakarta.inject.Inject;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Sep 5, 2016, 6:08:24 AM
 */
@ApplicationScoped
public class PropagatedContextFactory {

  @Any
  @Inject
  private Instance<AsynchronousContextSupplier<?>> asynchronousContextSuppliers;

  @Nonnull
  @Profile(logLevel = "FINE")
  public PropagatedContext createPropagatedContext() {
    final List<AsynchronousContext<?>> asynchronousContexts = ImmutableList
            .copyOf(asynchronousContextSuppliers)
            .stream()
            .map(AsynchronousContextSupplier::provide)
            .filter(Optional::isPresent)
            .map(Optional::get)
            .collect(Collectors.toList());

    return PropagatedContext
            .builder()
            .asynchronousContexts(asynchronousContexts)
            .build();
  }

}
