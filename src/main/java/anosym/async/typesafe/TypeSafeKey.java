package anosym.async.typesafe;

import java.io.Serializable;
import javax.annotation.Nonnull;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Jul 8, 2015, 6:17:42 PM
 */
public interface TypeSafeKey<T> extends Serializable {

    @Nonnull
    Class<T> getType();

    @Nonnull
    default String getKey() {
        return getType().getCanonicalName();
    }

}
