package anosym.async.typesafe;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Nonnull;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

import static java.lang.String.format;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Jul 8, 2015, 6:19:33 PM
 */
public final class TypeSafeKeys {

    @Nonnull
    public static TypeSafeKey<Boolean> booleanKey(@Nonnull final String keyName) {
        return new NamedTypeSafeKey<>(keyName, Boolean.class);
    }

    @Nonnull
    public static TypeSafeKey<Integer> integerKey(@Nonnull final String keyName) {
        return new NamedTypeSafeKey<>(keyName, Integer.class);
    }

    @Nonnull
    public static TypeSafeKey<Short> shortKey(@Nonnull final String keyName) {
        return new NamedTypeSafeKey<>(keyName, Short.class);
    }

    @Nonnull
    public static TypeSafeKey<Long> longKey(@Nonnull final String keyName) {
        return new NamedTypeSafeKey<>(keyName, Long.class);
    }

    @Nonnull
    public static TypeSafeKey<String> stringKey(@Nonnull final String keyName) {
        return new NamedTypeSafeKey<>(keyName, String.class);
    }

    @Nonnull
    public static TypeSafeKey<Character> characterKey(@Nonnull final String keyName) {
        return new NamedTypeSafeKey<>(keyName, Character.class);
    }

    @Nonnull
    public static TypeSafeKey<BigDecimal> bigDecimalKey(@Nonnull final String keyName) {
        return new NamedTypeSafeKey<>(keyName, BigDecimal.class);
    }

    @Nonnull
    public static <T> TypeSafeKey<List<T>> listKey(@Nonnull final String keyName, @Nonnull final Class<T> typed) {
        checkNotNull(keyName, "The keyName must not be null");
        checkNotNull(typed, "The typed must not be null");

        final List<T> list = new ArrayList<>();
        return new NamedTypeSafeKey<>(keyName, (Class<List<T>>) list.getClass());
    }

    @Nonnull
    public static <T> TypeSafeKey<List<T>> listKey(@Nonnull final Class<T> typed) {
        checkNotNull(typed, "The typed must not be null");

        return listKey(format("list##%s", typed.getCanonicalName()), typed);
    }

    @Nonnull
    public static <T> TypeSafeKey<T> key(@Nonnull final Class<T> typed) {
        checkNotNull(typed, "The typed must not be null");

        return key(typed.getCanonicalName(), typed);
    }

    @Nonnull
    public static <T> TypeSafeKey<T> key(@Nonnull final String keyName, @Nonnull final Class<T> type) {
        checkNotNull(keyName, "The keyName must not be null");
        checkNotNull(type, "The type must not be null");

        return new NamedTypeSafeKey<>(keyName, type);
    }

    @Getter
    @ToString
    @EqualsAndHashCode
    private static final class NamedTypeSafeKey<T> implements TypeSafeKey<T> {

        private static final long serialVersionUID = -1134195600126982165L;

        private final String key;

        private final Class<T> type;

        private NamedTypeSafeKey(@Nonnull final String key, @Nonnull final Class<T> type) {
            this.key = checkNotNull(key, "The key must not be null");
            this.type = checkNotNull(type, "The type must not be null");
        }

    }

}
