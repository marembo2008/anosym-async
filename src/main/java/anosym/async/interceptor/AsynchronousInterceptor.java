package anosym.async.interceptor;

import java.lang.reflect.Method;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import anosym.async.PropagatedContext;
import anosym.async.PropagatedContextExecutor;
import anosym.async.PropagatedContextFactory;
import jakarta.annotation.Priority;
import jakarta.enterprise.context.Dependent;
import jakarta.inject.Inject;
import jakarta.interceptor.AroundInvoke;
import jakarta.interceptor.Interceptor;
import jakarta.interceptor.InvocationContext;

import static java.lang.String.format;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Mar 12, 2016, 9:53:51 AM
 */
@Dependent
@Interceptor
@Asynchronous
@Priority(Interceptor.Priority.APPLICATION)
public class AsynchronousInterceptor {

  @Inject
  private ExecutorService executorService;

  @Inject
  private PropagatedContextExecutor asynchronousServicePropagatedContext;

  @Inject
  private PropagatedContextFactory propagatedContextFactory;

  @AroundInvoke
  public Object execute(@Nonnull final InvocationContext invocationContext) throws Exception {
    checkNotNull(invocationContext, "The invocationContext must not be null");

    final Method method = invocationContext.getMethod();
    final Class<?> returnType = method.getReturnType();
    checkState(returnType == void.class || returnType == Future.class,
               "Invalid asynchronous method call. Requires Void or Future return type");

    final PropagatedContext propagatedContext = propagatedContextFactory.createPropagatedContext();
    final Callable<?> callable = () -> invocationContext.proceed();
    return executorService.submit(() -> invoke(propagatedContext, callable));
  }

  @Nullable
  private Object invoke(@Nonnull final PropagatedContext propagatedContext, @Nonnull final Callable<?> callable) throws Exception {
    final Object result = asynchronousServicePropagatedContext.execute(propagatedContext, callable);
    if (result == null) {
      return result;
    }

    if (result instanceof Future) {
      return ((Future) result).get();
    }

    throw new IllegalStateException(format("Unexpected result type: %s", result.getClass()));
  }

}
