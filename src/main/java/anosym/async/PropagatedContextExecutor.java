package anosym.async;

import java.util.List;
import java.util.concurrent.Callable;

import javax.annotation.Nonnull;

import anosym.async.context.AsynchronousContext;
import anosym.async.context.AsynchronousContextConsumer;
import anosym.async.context.AsynchronousContextRemover;
import anosym.async.context.ForAsynchronousContext;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.enterprise.inject.Any;
import jakarta.enterprise.inject.Instance;
import jakarta.enterprise.util.AnnotationLiteral;
import jakarta.inject.Inject;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * @author marembo (marembo2008@gmail.com)
 * @since Jul 6, 2016, 8:03:11 PM
 */
@ApplicationScoped
public class PropagatedContextExecutor {

  @Any
  @Inject
  private Instance<AsynchronousContextConsumer<?>> asynchronousContextConsumers;

  @Any
  @Inject
  private Instance<AsynchronousContextRemover<?>> asynchronousContextRemovers;

  public <T> T execute(@Nonnull final PropagatedContext propagatedContext, @Nonnull final Callable<T> callable) {
    checkNotNull(callable, "The callable must not be null");

    try {
      setUp(propagatedContext);

      return callable.call();
    } catch (final Exception e) {
      throw new RuntimeException(e);
    } finally {
      cleanUp(propagatedContext);
    }
  }

  public void execute(@Nonnull final PropagatedContext propagatedContext, @Nonnull final Runnable task) {
    checkNotNull(task, "The task must not be null");

    try {
      setUp(propagatedContext);

      task.run();
    } catch (final Exception e) {
      throw new RuntimeException(e);
    } finally {
      cleanUp(propagatedContext);
    }
  }

  private void setUp(@Nonnull final PropagatedContext propagatedContext) {
    final List<AsynchronousContext<?>> asynchronousContexts = propagatedContext.getAsynchronousContexts();
    if (asynchronousContexts != null) {
      asynchronousContexts
              .stream()
              .forEach(this::notifyConsumers);
    }
  }

  private void notifyConsumers(@Nonnull final AsynchronousContext asynchronousContext) {
    final Class subType = asynchronousContext.getContextKey().getType();
    final ForAsynchronousContext forAsynchronousContext = new ForAsynchronousContextImpl(subType);
    asynchronousContextConsumers
            .select(forAsynchronousContext)
            .forEach((asynchronousContextConsumer) -> {
              asynchronousContextConsumer.consume(asynchronousContext);
            });

  }

  private void cleanUp(@Nonnull final PropagatedContext propagatedContext) {
    final List<AsynchronousContext<?>> asynchronousContexts = propagatedContext.getAsynchronousContexts();
    if (asynchronousContexts != null) {
      asynchronousContexts
              .stream()
              .forEach(this::notifyRemovers);
    }
  }

  private void notifyRemovers(@Nonnull final AsynchronousContext asynchronousContext) {
    final Class subType = asynchronousContext.getContextKey().getType();
    final ForAsynchronousContext forAsynchronousContext = new ForAsynchronousContextImpl(subType);
    asynchronousContextRemovers
            .select(forAsynchronousContext)
            .forEach((asynchronousContextRemover) -> {
              asynchronousContextRemover.remove(asynchronousContext);
            });

  }

  @SuppressWarnings("AnnotationAsSuperInterface") //CDI
  private static final class ForAsynchronousContextImpl extends AnnotationLiteral<ForAsynchronousContext> implements ForAsynchronousContext {

    private static final long serialVersionUID = 64288748110137480L;

    private final Class<?> contextClass;

    public ForAsynchronousContextImpl(@Nonnull final Class<?> contextClass) {
      this.contextClass = contextClass;
    }

    @Override
    public Class<?> value() {
      return contextClass;
    }

  }

}
