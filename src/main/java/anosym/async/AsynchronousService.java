package anosym.async;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;

import javax.annotation.Nonnull;

import jakarta.ejb.Stateless;
import jakarta.inject.Inject;

import static com.google.common.base.Preconditions.checkNotNull;

/*
 * import javax.ejb.Stateless;
 *
 *
 * @author marembo (marembo2008@gmail.com) @since Mar 12, 2016, 9:53:51 AM
 */
@Stateless
public class AsynchronousService {

  @Inject
  private ExecutorService executorService;

  @Inject
  private PropagatedContextExecutor asynchronousServicePropagatedContext;

  @Inject
  private PropagatedContextFactory propagatedContextFactory;

  public <T> Future<T> execute(@Nonnull final Callable<T> callable) {
    checkNotNull(callable, "The callable must not be null");

    final PropagatedContext propagatedContext = propagatedContextFactory.createPropagatedContext();
    return executorService.submit(()
            -> asynchronousServicePropagatedContext.execute(propagatedContext, callable));
  }

  public void execute(@Nonnull final Runnable task) {
    checkNotNull(task, "The task must not be null");

    final PropagatedContext propagatedContext = propagatedContextFactory.createPropagatedContext();
    executorService.submit(() -> asynchronousServicePropagatedContext.execute(propagatedContext, task));
  }

}
