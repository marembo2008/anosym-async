package anosym.async.context;

import javax.annotation.Nonnull;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Aug 31, 2016, 6:58:39 PM
 */
public interface AsynchronousContextRemover<T> {

    @Nonnull
    void remove(@Nonnull final AsynchronousContext<T> asynchronousContext);
}
