package anosym.async.context;

import java.util.Optional;

import javax.annotation.Nonnull;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Aug 31, 2016, 6:58:39 PM
 */
public interface AsynchronousContextSupplier<T> {

    @Nonnull
    Optional<AsynchronousContext<T>> provide();

}
