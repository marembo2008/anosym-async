package anosym.async.context;

import java.util.Optional;

import javax.annotation.Nonnull;

import anosym.async.typesafe.TypeSafeKey;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.ToString;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Aug 31, 2016, 7:00:36 PM
 */
@Getter
@Builder
@ToString
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class AsynchronousContext<T> {

    @Nonnull
    private final TypeSafeKey<T> contextKey;

    @Nonnull
    private final T context;

    @Nonnull
    public Optional<AsynchronousContext<T>> toOptional() {
        return Optional.of(this);
    }

}
