package anosym.async;

import java.util.List;

import javax.annotation.Nullable;

import anosym.async.context.AsynchronousContext;
import lombok.Builder;
import lombok.Data;
import lombok.Singular;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Jul 6, 2016, 10:01:59 PM
 */
@Data
@Builder
public class PropagatedContext {

    @Nullable
    @Singular
    private final List<AsynchronousContext<?>> asynchronousContexts;

}
