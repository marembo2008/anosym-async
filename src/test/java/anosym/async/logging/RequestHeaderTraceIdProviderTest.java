package anosym.async.logging;

import org.junit.jupiter.api.Test;

import anosym.async.logging.RequestHeaderTraceIdProvider;
import anosym.async.logging.TraceId;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 */
public class RequestHeaderTraceIdProviderTest {

  @Test
  public void testSomeMethod() {
    final RequestHeaderTraceIdProvider traceIdProvider = new RequestHeaderTraceIdProvider();
    traceIdProvider.setTraceId(TraceId.createTraceId());
  }

}
